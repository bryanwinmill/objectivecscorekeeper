//
//  ViewController.m
//  ObectiveCScoreKeeper
//
//  Created by Bryan Winmill on 9/21/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *TeamTwoScore;

@property (strong, nonatomic) IBOutlet UITextField *TeamTwoName;

@property (strong, nonatomic) IBOutlet UILabel *TeamOneScore;

@property (strong, nonatomic) IBOutlet UITextField *TeamOneName;

@property (strong, nonatomic) IBOutlet UIStepper *ResetStepperOne;

@property (strong, nonatomic) IBOutlet UIStepper *ResetStepperTwo;

@end

@interface TeamOneName : UITextField <UITextFieldDelegate>
@end
@interface TeamTwoName : UITextField <UITextFieldDelegate>
@end

@implementation ViewController

- (IBAction)enableTeamOneText:(UITextField *)sender {
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)TeamTwoStepper:(UIStepper *)sender {
    
    _TeamTwoScore.text = [NSString stringWithFormat: @"%.0f", sender.value];
}

- (IBAction)TeamOneStepper:(UIStepper *)sender {
    
    _TeamOneScore.text = [NSString stringWithFormat: @"%.0f", sender.value];
}

- (IBAction)ResetScores:(UIButton *)sender {
    
    _TeamOneScore.text = @"0";
    _TeamTwoScore.text = @"0";
    _ResetStepperOne.value = 0.0;
    _ResetStepperTwo.value = 0.0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.TeamOneName.delegate = self;
    self.TeamTwoName.delegate = self;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
