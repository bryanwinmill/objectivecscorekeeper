//
//  AppDelegate.h
//  ObectiveCScoreKeeper
//
//  Created by Bryan Winmill on 9/21/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

